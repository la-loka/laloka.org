---
layout: base-layout.njk
title: Donacions
---

<p>Si pots i et ve de gust, finança econòmicament l'associació amb una donació puntual o mensual.</p>
<ul class="a">
<li>Amb moneda lliure Ğ1 (crear compte no membre per LaLoka)</li>
<li>Amb euros</li>
<ul class="a">
<li>A través d'OpenCollective<br />
<a href="https://opencollective.com/laloka">OpenCollective de LaLoka</a></li>
<li>A través d'una transferència bancària al compte corrent de l'associació: <b>ES00 0000 0000 0000 0000</b></li>
<li>Per correu postal:
<p>Associació per la Cultura Lliure LaLoka<br />
c/ nº<br />
cp:<br />
Barcelona, Catalunya, Spain</p></li>
</ul>
</ul>
