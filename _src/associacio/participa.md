---
layout: base-layout.njk
title: Participa
---

Pots donar suport a l'associació de diverses maneres.

## Difusio
Informar també es participar. Parla o escriu sobre l'associació i els seus projectes. Si tens un blog, dedica'ns unes línies i enllaça a la nostra web. I si no, aprofita les sobretaules per explicar a les persones que estimes la importància de la cultura lliure per a construir aquest nou món que portem a les nostres eines.

## Voluntariat
Sempre es necessiten mans per tirar endavant els projectes, per exemple, traduint o corregint en català o altres idiomes. També es pot donar un cop de mà fent més accessibles els continguts web: posant subtítols a vídeos o fent transcripcions d'àudios. Si t'engresquen aquestes tasques, registra't a l'àgora de Fedicat perquè ens poguem coordinar.

<a href="https://agora.fedi.cat">Àgora de Fedicat</a>

## Altres suports
<ul class="a">
<li><a href="socies.html">Associa't</a></li>
<li><a href="donacions.html">Fés una donació</a></li>
<li><a href="formacions.html">Sol·licita formacions</a></li>
<li><a href="https://liberaforms.org/ca/serveis">Contracta un servei de formularis</a></li>
</ul>

## Propostes
Si vols fer una col·laboració amb l'associació, escriu-nos un correu o fes la proposta per l'àgora Fedicat.
