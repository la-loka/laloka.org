---
layout: base-layout.njk
title: L'associacio
---

Amb el lema: &quot;CULTURA LLIURE, SOCIETAT LLIURE!&quot;, l'associació LaLoka promou eines i continguts lliures en català. A LaLoka, usem, difonem i creem béns comuns digitals.

## Objectius

L'associació LaLoka té per objectius:

<ol start="0">
<li><b>Difondre</b> les diverses manifestacions de la Cultura lliure principalment en llengua catalana</li>
<li><b>Gestionar</b> la pròpia associació amb programari lliure per servir d’exemple a altres organitzacions</li>
<li><b>Facilitar</b> la comprensió de la descentralització web i l’ús d’eines descentralitzades</li>
<li><b>Desenvolupar</b> projectes de Cultura lliure i afavorir la col·laboració entre projectes ja existents</li>
</ol>

## Accions

Per assolir els objectius de l'associació es contemplen accions com:

* Recopilació de bibliografia sobre l’estat i l’ús del programari lliure
* Promoció d’accions culturals a barris i pobles
* Oferir infraestructures per a l’ús d’eines lliures d’interès general
* Organització d’activitats divulgatives i formatives
* Creació de documentació i manuals d’ús de programari lliure
* Desenvolupament de projectes propis relacionats amb la Cultura lliure
* Impuls i divulgació de projectes alineats amb els objectius de l'associació


## Documentacio

### Documents oficials

* Acta fundacional
* [Estatuts](/associacio/estatuts/)
* Acords de convivència


### Documents de comunicacio

<ul class="a">
<li>Logo en diferents formats</li>
<li>Altres documents gràfics: fulletons, cartells, il·lustracions</li>
</ul>


### Altres documents

<ul class="a">
<li><a href="/recursos/">Recursos</a></li>
</ul>


## Activitat de l'associacio

<ul class="a">
<li><a href="/mitjans-socials/">Mitjans socials</li>
<li><a href="https://agora.fedi.cat">Àgora de participació</a></li>
</ul>


## Serveis

### Servei de formularis
Des de LaLoka, impulsem el desenvolupament de projectes com LiberaForms, un programari per crear formularis ètics.

<ul>
<li><a href="https://liberaforms.org/ca/serveis">Contracta un servei de formularis amb LiberaForms</a></li>
<li><a href="/projectes/">Descobreix altres projectes de LaLoka</a></li>
<li><a href="/eines/">Inspira't de les nostres eines</a></li>
</ul>

### Formacions i assessoraments

<ul>
<li>Si vols organitzar una activitat sobre cultura lliure a la teva entitat
<a href="/formacions/">consulta les nostres formacions</a></li>
<li>Si necessites assessorament o alguna formació a mida, contacta'ns per correu electrònic</li>
</ul>

## Dona'ns suport
<p>Si t'agrada el que fem, ens pots donar suport de diverses maneres.</p>
<ul>
<li><a href="/associacio/socies/">Fes-te sòcia</a></li>
<li><a href="/associacio/participa/">Participa-hi altrament</a></li>
</ul>

## Contacta'ns
<ul class="a">
<li>Escriu-nos un correu electrònic</li>
<li>Contacta'ns per la Fedi</li>
<li>Escriu-nos una carta</li>
<p>Associació per la Cultura Lliure LaLoka<br />
c/ nº<br />
cp:<br />
Barcelona, Catalunya, Spain</li>
</ul>
