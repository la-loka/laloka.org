---
layout: base-layout.njk
title: Estatuts de LaLoka
---


<ul>
<li>Versió dels Estatuts en odt</li>
<li>Versió dels Estatuts en pdf</li>
</ul>
<h2 id="capitol-i.-la-denominacio-els-fins-i-el-domicili">Capitol I. La denominacio, els fins i el domicili</h2>
<h3 id="preambul">Preambul</h3>
<p>L’Associació per la cultura lliure &quot;LaLoka&quot; és una associació que vol promoure les diverses manifestacions de la cultura lliure en llengua catalana, especialment eines i recursos de programari lliure, i molt especialment, descentralitzades i federades.</p>
<h3 id="article-1.-denominacio">Article 1. Denominacio</h3>
<p>L’Associació per la cultura lliure &quot;LaLoka&quot;, en endavant LaLoka, és una associació sense ànim de lucre que regularà les seves activitats d'acord amb la Llei 4/2008, de 24 d’abril, del Llibre Tercer del Codi Civil de Catalunya, relatiu a les persones jurídiques; Llei orgànica 1/2002, de 22 de març, reguladora del dret associació, i els seus estatuts.</p>
