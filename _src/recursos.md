---
layout: base-layout.njk
title: Recursos
---

<p>Molts continguts sobre cultura lliure només estan disponibles en idiomes que no són el català. Per això, des de l'Associació LaLoka volem crear i recrear continguts per apropar la cultura lliure a les persones catalanoparlants. Ajuda'ns a traduir, corregir, subtitular i transcriure continguts al català. Registra't a l'àgora de Fedicat per participar en la creació i recreació de continguts sobre cultura lliure. Els recursos que anem creant i recreant els endrecem als nostres Arxius Oberts.</p>
<ul class="a">
<li><a href="https://agora.fedi.cat">Àgora Fedicat</a></li>
<li><a href="https://arxius.laloka.org/w/recursos/">Recursos a Arxius Oberts</a></li>
</ul>
<h2 id="traduccions">Traduccions</h2>
<p>Pesquem textos en altres idiomes que creiem que valen la pena traduir. Si trobes un text interessant sobre la descentralització o els mitjans federats, avisa'ns per la Fedi o per l'àgora. I si necessites ajut per traduir-ne un, obre un fil a l'àgora.</p>
<h2 id="correccions">Correccions</h2>
<p>Sempre és bo detectar i corregir errors tipogràfics, lapsus calami o faltes d'ortografia. Si detectes errades en un text, comenta'ns-ho per l'àgora o per correu electrònic.</p>
<h2 id="subtitulat">Subtitulat</h2>
<p>Una gran barrera dels continguts digitals és l'idioma. Ajuda'ns a subtitular vídeos en català. A més de ser útils per a comprendre un altre idioma, el subtitulat d'un vídeo també és útil per algunes persones amb diversitat funcional. Participa a l'àgora de la Fedicat i dona'ns un cop de mà.</p>
<h2 id="transcripcions">Transcripcions</h2>
<p>En un món ideal, tot contingut té la seva transcripció. Volem que els idiomes i continguts siguin ponts, i no barreres. Ajuda'n a transcriure continguts.</p>
