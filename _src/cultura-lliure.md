---
layout: base-layout.njk
title: Cultura lliure
---

<p>Quan parlem de Cultura lliure, incloem tant les eines com els continguts. A l'associació LaLoka, usem programari lliure i llicenciem les nostres creacions amb llicències lliures, ja siguin programes, textos, imatges, àudios o vídeos.</p>
<h2 id="definició-de-cultura-lliure">Definicio de cultura lliure</h2>
<p>Segons la Viquipèdia, la Cultura lliure és <em>tota aquella creació, i el moviment que la promou, que advoca per l'elaboració i difusió de cultura d'acord amb uns principis de llibertat equiparables als del programari lliure.</em></p>
<p><a href="https://ca.wikipedia.org/wiki/Cultura_lliure">Definició de Cultura lliure a la Viquipèdia</a></p>
<h3 id="les-llibertats-del-programari-lliure">Les llibertats del programari lliure</h3>
<p>Són aquestes quatre:</p>
<ol class="b" start="0">
<li><b>Usar</b> el programa com es vulgui</li>
<li><b>Estudiar</b> el codi del programa per aprendre com funciona i poder-lo adaptar a les teves necessitats</li>
<li><b>Compartir</b> el programa amb qui vulguis</li>
<li><b>Millorar</b> el programa i compartir les millores públicament perquè tothom se'n pugui beneficiar</li>
</ol>
<p><a href="https://www.gnu.org/philosophy/free-sw.html">Definició de Programari lliure al projecte GNU</a></p>
<h2 id="llicències-de-contingut">Llicencies de contingut</h2>
<p>Per garantir les llibertats del programari i els continguts que creem, posem llicències a les nostres obres.</p>
<h3 id="per-al-programari">Per al programari</h3>
<p>Per als nostres programes usem Copyleft, és a dir, que garantim que el programa que creem sigui lliure per sempre més. Paradoxalment, una llicència Copyleft, obliga a les persones que modifiquen un programa a llicenciar el programa amb la mateixa llicència. I així, ens assegurem que tant el programa original com les seves modificacions són sempre lliures.</p>
<p><a href="https://www.gnu.org/licenses/copyleft.html">Definició de Copyleft al projecte GNU</a></p>
<h3 id="per-al-contingut">Per al contingut</h3>
<p>Per als nostres continguts, si no s'especifica altrament, usem la llicencia Creative Commons Atribució i Compartir igual (CC BY-SA). Com la GPL o l'AGPL per al programari, la CC BY-SA és una llicència Copyleft per als continguts. Permet usar i reusar el contingut sempre i quan es facin dues coses: citar a qui ha creat el contingut i posa la mateixa llicència a l'obra que se'n derivi.</p>
<p><a href="https://creativecommons.org/">Llicències Creative Commons</a></p>
