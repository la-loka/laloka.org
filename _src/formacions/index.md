---
layout: base-layout.njk
title: Formacions
---

Proposem xerrades, tallers i cursos per promoure i posar en pràctica la cultura lliure.

## Xerrades
Les xerrades introductòries tenen una durada d'una hora i són sessions teòriques on s'exposen conceptes relacionats amb la cultura lliure. Es fan en dues parts: uns quaranta minuts d'exposició de la temàtica i uns vint minuts de debat o preguntes.

<ul class="a">
<li>Introducció al programari lliure (enllaç a fitxa)</li>
<li>Introducció als mitjans socials federats (enllaç a fitxa)</li>
</ul>

## Tallers

<p>Els tallers de demostració tenen una durada de dues hores i són sessions pràctiques per tractar accions més específiques.</p>
<ul class="a">
<li>Crea materials gràfics amb programari lliure (enllaç a fitxa)</li>
<li>Mitjans socials federats: funcionament i bones pràctiques (enllaç a fitxa)</li>
</ul>

## Curs de comunicacio amb programari lliure

Aquest curs d'aprofundiment té una durada de sis hores repartides en quatre sessions d'una hora i mitja. Es pot fer concentrat en dues setmanes (dues sessions d'una hora i mitja per setmana) o pausat en un mes (una sessió d'una hora i mitja per setmana). Hi ha dues sessions teòriques i dues sessions pràctiques.

<a href="/formacions/comunicacio/">Consulta la fitxa del curs</a>

#Virtual i presencial

<p>Les formacions es poden fer de forma virtual i també presencialment si el desplaçament es dóna a l'àrea metropolitana de Barcelona. Si necessiteu que ens desplacem més enllà o voleu una formació a mida, envieu-nos un correu electrònic.</p>

### Formacions virtuals

<p>Requeriments per a les formacions virtuals:</p>
<ul class="a">
<li>Necessitareu connexió a Internet</li>
<li>Proporcionarem l'eina Big Blue Button</li>
</ul>
<p>Us farem arribar un enllaç que us durà a la sala virtual de formació. Un cop arribeu a la sala, haureu d'assignar-vos un nom o pseudònim i ja hi podreu entrar.</p>

### Formacions presencials

<p>Requeriments per a les formacions presencials:</p>
<ul class="a">
<li>Proposeu-nos un espai adequat al nombre de persones assistents</li>
<li>Necessitarem electricitat i, si pot ser, connexió a Internet</li>
<li>Ens caldrà un projector o pantalla que funcionin amb cable VGA i altaveus</li>
</ul>
<p>Sol·licita una formació (enllaç a formulari)</p>
</ul>
