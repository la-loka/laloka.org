---
layout: base-layout.njk
title: Comunicacio amb programari lliure
---

En aquest curs reflexionarem sobre el context digital i coneixerem eines de programari lliure per crear i publicar continguts. A més, descobrirem les llicències Creative Commons.

<h2 id="dades-del-curs">Dades del curs</h2>
<ul class="a">
<li>Durada: 6 hores</li>
<li>Sessions: 4</li>
<li>Persones destinatàries: membres de juntes directives i persones responsables de la comunicació</li>
<li>Modalitat: sessions en línia o presencials</li>
<li>Metodologia: curs teòric i pràctic</li>
</ul>

<h2 id="objectius">Objectius</h2>
<ul class="a">
<li>Reflexionar sobre el context digital actual</li>
<li>Fomentar l'ús d'eines digitals lliures en la creació de contingut</li>
<li>Apropar-se a les llicències de contingut lliures</li>
<li>Descobrir la Federació de mitjans socials descentralitzats</li>
</ul>

<h2 id="contingut">Contingut</h2>
<ol class="b">
<li>Les GAFAM: un problema glocal</li>
<li>Fluxos de treball i creativitat amb programari lliure</li>
<li>Com llicenciar una obra amb llicències creative commons</li>
<li>&quot;El mitjà és el missatge&quot;: com fomentar una participació digital qualitativa</li>
</ol>

Sol·licita aquesta formació (enllaç a formulari)</br>
<a href="/formacions/">Consulta les altres formacions</a>
