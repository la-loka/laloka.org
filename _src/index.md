---
layout: base-layout.njk
title: Benvinguda
---

<p>LaLoka és una associació sense ànim de lucre que promou la <b>cultura lliure en català</b>, especialment continguts i eines de programari lliure, i molt especialment, tecnologies descentralitzades i federades.</p>
<p>LaLoka, La Local en esperanto, gestiona íntegrament la seva infraestructura amb eines lliures i participa del moviment del programari lliure amb projectes i col·laboracions de diversa naturalesa.</p>
<h2 id="per-què">Per que</h2>
<p>Vivim en un món on les tecnologies digitals són com un líquid que s'escola imparable per tot arreu. I si bé el salt digital a les Internets havia de ser una pluja d'oportunitats per a tothom, el context digital ha estat capitalitzat per poques corporacions. No totes les tecnologies digitals existents són iguals, i algunes són més ètiques que altres en el seu disseny i pràctiques.</p>
<p>A LaLoka creiem que <b>un altre paisatge digital no només és possible</b>: ja ha existit i segueix existint. És més, creiem que tot projecte que vol posar les persones al centre i dur a terme una transformació social coherent, hauria d'usar i promocionar tecnologies ètiques.</p>
<h2 id="cultura-lliure-societat-lliure">Cultura lliure, societat lliure!</h2>
<ul class="a">
<li><a href="cultura-lliure.html">Què entenem per Cultura lliure</a></li>
<li><a href="associacio.html">Coneix millor l'associació</a></li>
<li><a href="projectes.html">Descobreix els nostres projectes</a></li>
<li><a href="eines.html">Inspira't de les nostres eines</a></li>
<li><a href="socies.html">Fes-te sòcia de LaLoka</a></li>
<li><a href="participa.html">Participa-hi altrament</a></li>
</ul>
<p>Si no s'indica el contrari, els continguts d'aquesta pàgina web estan sota llicència CC BY-SA.</p>
