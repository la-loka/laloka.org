---
layout: base-layout.njk
title: Projectes
---

Des de LaLoka creem i dinamitzem projectes relacionats amb la cultura lliure.

## Fedicat
El projecte Fedicat vol apropar els mitjans socials lliures a la comunitat catalanoparlant. Sota aquest domini, hi tenim una pàgina web explicativa, una àgora de participació i un fediblog. A més, des del projecte Fedicat, impulsem tres accions concretes en relació als mitjans socials lliures federats.

[Visita la pàgina web de Fedicat](https://fedi.cat)

### Observatori
L'observatori recull dades públiques sobre l'ús de mitjans socials federats a l'Economia Social i Solidària. Implica un informe i una infografia. Si hi vols col·laborar, registra't a l'àgora de participació de Fedicat.

[Participa a l'àgora de Fedicat](https://agora.fedi.cat)

### Publicacio mensual
Des del compte <span class="citation">@fedizens</span> del Fediblog, publicarem una entrada mensual amb informació sobre la Fedivers.

[Llegeix el Fediblog](https://blog.fedi.cat)

### Curs de comunicacio amb programari lliure

Hem dissenyat un curs de comunicació per a reflexionar, crear i compartir continguts digitals amb tecnologies lliures i descentralitzades.
<a href="/formacions/comunicacio/">Consulta la fitxa del curs</a>

## Barcelona social

El node de Barcelona.social és una porta oberta a l'ecosistema glocal de la Federació de mitjans socials lliures. Barcelona.social dóna servei de microblogging federat a través de l'eina Pleroma, és a dir, que si hi tens un compte, podràs interactuar amb veïnes locals i internacionals independentment de l'eina que facin servir, ja sigui que publiquin imatges, vídeos o textos. Per exemple, des de Pleroma pots comentar un vídeo de Peertube o una foto de Pixelfed.

Però això, els mitjans asocials privatius no t'ho deixen fer perquè cada corporació vol recollir dades al seu jardí vallat. A dia d'avui, ni des de Twitter ni des de Facebook pots comentar vídeos de Youtube o fotos d'Instagram. Vine a la Fedi i passa de les GAFAM!

Si vius per Barcelona o tens vincles amb la ciutat o les seves gents, et convidem a llegir els acords de convivència del node. Si hi estàs d'acord, ens encantarà que vinguis a fer pinya.

* <a href="https://barcelona.social/about">Llegeix els acords de convivència de barcelona.social</a>
* <a href="https://opencollective.com/laloka/contribute/projecte-barcelona-social-24996/checkout">Fes una donació per mantenir el node</a>


## Programari
A LaLoka també desenvolupem programari.

### LiberaForms
El projecte LiberaForms tracta del desenvolupament d'un programa que permet crear i administrar formularis de forma fàcil i ètica. LiberaForms és programari de quilòmetre zero i cultura lliure de proximitat.

[Coneix més a fons el projecte LiberaForms](https://liberaforms.org)


### Arxius Oberts
Un programa que ens permet endreçar la documentació pública altrament, des d'una web que mostra el contingut que es posa a un directori de Nextcloud. A més de funcional, aquest programa és elegant i robust. Si vols instal·lar-lo o trastejar el codi, passa per la forja.

[Consulta la documentació d'Arxius Oberts](https://gitlab.com/la-loka/arxiusoberts)


## MIXETESS
El projecte MIXETESS respon a l'acrònim Moviment per unes Infraestructures i Xarxes Ètiques: Transparents, Ecològiques, Sobiranes i Solidàries. Tal com fan els CHATONS francesos, les MIXETESS catalanes volen evitar la recol·lecció i centralització de les dades personals en empreses GAFAM (Google, Apple, Facebook, Amazon, Microsoft). Estem treballant en aquest projecte poquet a poquet.

De fet, LaLoka és una mixeta. Des de LaLoka i els seus projectes oferim serveis web com documents col·laboratius, audioconferències, l'àgora de participació de Fedicat i el node fediversal de microblogging barcelona.social. A més, disposem d'un servidor de pàgines web estàtiques, d'un de blog federat i d'una instància personalitzada de LiberaForms per a crear formularis ètics.

* <a href="https://chatons.org">Inspira't dels CHATONS</a>
* En construcció, <a href="https://mixetess.org">Pàgina web MIXETESS</a>
* <a href="eines.html">Inspira't de les nostres eines</a>


## Col·laboracions
Algunes de les nostres sòcies també participen en altres associacions, comunitats i projectes vinculats a la cultura lliure. Volem mencionar especialment a:

* <a href="https://exo.cat">L'associació eXO</a>
* <a href="https://anartist.org">La comunitat Anartist</a>
* <a href="https://monedalliure.org">El projecte Moneda Lliure</a>
