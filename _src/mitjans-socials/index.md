---
layout: base-layout.njk
title: Mitjans socials
---

<p>Hi ha dos tipus de mitjans digitals: uns d'asocials privatius que són corporatius i centralitzats, i uns de socials lliures que són comunitaris i federats. A l'associació LaLoka només participem de la Federació de mitjans socials lliures i federats, la Fedivers, una navalla suïssa de la comunicació lliure.<br />
<a href="https://fedi.cat">Coneix millor la Fedivers</a></p>
<p>A més, a LaLoka administrem i moderem el nostre propi node de Pleroma<br />
Saluda'ns a la Fedi (crear compte fedi)</p>
<p>El projecte Fedicat també participa a la Fedi amb un node de Plume.<br />
<a href="https://blog.fedi.cat/@/fedizens">Llegeix el blog de les fedizens</a></p>
<h2 id="mes-enlla-de-la-rap">Mes enlla de la RAP</h2>
<p>Resistència a l'Agressió Publicitària, coneguda com a RAP, és una associació francesa que és conscient de la toxicitat dels mitjans asocials privatius i s'autoimposa tres restriccions en el seu ús. Si sou una organització en defensa dels drets humans, una administració pública, lluiteu per la transformació social o simplement voleu contribuir a un món millor amb els vostres clics, us recomanem les restriccions de la RAP:</p>
<ol class="b">
<li>No fer promoció de xarxes socials publicitàries</li>
<li>Evitar posar contingut exclusiu a les xarxes socials publicitàries</li>
<li>No recórrer a l'esponsorització</li>
</ol>
<p><a href="/mitjans-socials/rap-restriccions-ca/">Traducció de les restriccions de la RAP</a></p>
<p>A LaLoka anem molt més enllà d'aquestes restriccions i, directament, no usem mitjans privatius. Si vols estar al dia del que fem, informa't a la nostra web i al nostre compte de mitjans socials lliures.<br />
<a href="/mitjans-socials/efecte-xarxa/">Sobre l'efecte xarxa</a>
