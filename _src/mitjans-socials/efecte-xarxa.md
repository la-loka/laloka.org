---
layout: base-layout.njk
title: Sobre l'efecte xarxa
---

<h2 id="la-web-casa-nostra">La Web, casa nostra</h2>
<p>Considerem que per conèixer un projecte potser sí que, avui dia, cal una pàgina web però no cal un compte a mitjans privatius. I estar a una pàgina de Facebook no és participar de la Web, és regalar-li a una empresa el nostre contingut, les nostres converses. Participar de la Web és tenir la teva pròpia pàgina sota un domini i un servidor que controlis tu i/o algú de la teva confiança. Una pàgina web ha de ser la teva casa digital, un lloc on hi posis les coses que vols compartir públicament. I Facebook, per exemple, no és públic, és privat. Si no hi tens compte no pots veure les publicacions.</p>
<p>Participant de mitjans privatius, privem a altres persones d'accedir als nostres continguts, enriquint encara més els que ja són rics. Evita publicar contingut exclusiu en mitjans privatius. Publica els teus continguts a la teva pàgina web perquè els pugui veure tothom i no només algunes persones en un jardí vallat.</p>
<h2 id="un-nou-efecte-xarxa">Un nou efecte xarxa</h2>
<h3 id="icones">Icones</h3>
<p>Si algú arriba a la nostra casa digital i es troba que només participem de mitjans corporatius és probable que acabi anant allà per interactuar amb nosaltres. En canvi, si participem de mitjans lliures, donarem l'oportunitat de conèixer altres maneres de fer. Participar de mitjans socials lliures no és tenir un altaveu disponible les 24 hores del dia, és relacionar-se amb altres persones, fomentar debats, fer pinya. Usa mitjans socials federats i posa les icones de les eines que usis a la teva pàgina web per generar un nou efecte xarxa.</p>
<h3 id="parametres">Parametres</h3>
<p>Participar quantitativament és comptar retuits o analitzar les visites d'una pàgina, però participar qualitativament implica fomentar i alimentar debats constructius, on diverses veus hi diguin la seva amb respecte. Tal com s'intenta fer a la vida física. Però la problemàtica dels mitjans federats, com passa amb la resta del programari lliure, és que no tenim publicistes i, si no es proven durant al menys uns mesos, no es poden començar a apreciar les diferències amb els mitjans asocials privatius. Usa mitjans socials federats per descobrir què volem dir per participació digital qualitativa.</p>
<h3 id="mencions">Mencions</h3>
<p>Si has de mencionar a algú, fes-ho a mitjans socials federats per crear i reforçar enllaços forts. Parlem d'enllaços forts en dos sentits: l'enllaç de la pròpia <a href="https://ca.wikipedia.org/wiki/Localitzador_uniforme_de_recursos">URL</a> i l'enllaç que es genera entre les persones a través de la interacció. Usem mitjans federats per fomentar un nou efecte xarxa!</p>
