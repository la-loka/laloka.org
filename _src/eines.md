---
layout: base-layout.njk
title: Eines
---

<p>A LaLoka fem servir eines de programari lliure tant per organitzar-nos com per crear i publicar continguts.</p>

## Eines organitzatives
<p>Per organitzar l'associació i els seus projectes:</p>
<ul class="a">
<li>Gestionem els nostres dominis amb Gandi i Don Dominio</li>
<li>Els servidors els tenim amb eXO, Maadix i ScaleWay</li>
<li>Mantenim un servidor de correu</li>
<li>Tenim muntat un servidor de pàgines web amb <a href="https://getgrav.org">Grav</a></li>
<li>Emmagatzemem la nostra informació amb <a href="https://nextcloud.com">Nextcloud</a></li>
<li>Endrecem la documentació pública amb <a href="https://gitlab.com/la-loka/arxiusoberts">Arxius Oberts</a></li>
<li>Fem còpies de seguretat de la nostra informació</li>
</ul>

## Altres eines que gestionem
<p>Proporcionem algunes eines que només requereixen registrar-se amb un correu electrònic, d'altres necessiten d'una petició prèvia, i algunes altres es poden fer servir sense registrar-se enlloc ni fer cap petició.</p>

### Eines amb peticio
<p>Per usar aquestes eines s'ha de ser sòcia i fer una petició d'ús. Un cop acceptada la petició, et crearem un compte.</p>
<ul class="a">
<li><b>Formularis amb LiberaForms</b><br />
Des de LaLoka, donem suport al desenvolupament del programari lliure LiberaForms, una eina per a crear formularis que respecten els drets digitals de les persones que en fan ús. Si ets sòcia de LaLoka, t'oferim fer formularis des del domini &quot;liberaforms.laloka.org&quot;.</p></li>
<ul class="a">
<li><a href="https://liberaforms.org">Projecte LiberaForms</a></li>
<li>Si ets sòcia i vols usar LiberaForms, omple el formulari de petició per als formularis (enllaç a petició)</p></li>
</ul>
<li><b>Blog federat amb Plume</b><br />
A LaLoka usem i promocionem eines de publicació descentralitzades i federades. És més, tenim un projecte específic anomenat Fedicat per donar-les a conèixer. Si ets sòcia, t'oferim publicar un blog des del domini &quot;blog.fedi.cat&quot;.</p></li>
<ul class="a">
<li><a href="https://fedi.cat">Projecte Fedicat</a></li>
<li>Si ets sòcia i vols usar el Plume, omple el formulari de petició per al fediblog (enllaç a petició)</li>
</ul>
</ul>

### Eines amb registre sense peticio
<p>Per usar aquestes eines no cal ser sòcia ni fer una petició d'ús, però fer-les servir implica llegir i acceptar els <a href="/associacio/acords/">acords de convivència de l'associació</a>. Et pots registrar a aquestes eines amb un correu electrònic però estarem molt agraïdes si aquest correu no depèn de cap multinacional.</p>
<ul class="a">
<li><b>Àgora Fedicat amb Discourse</b><br />
L'àgora és un espai de debat públic i també d'organització interna. És una eina que considerem molt útil perquè ens permet endreçar temes per categories, fer seguiment de propostes, proposar enquestes, etc. A més, es pot usar com a missatgeria i com a llista de correu. Si t'interessa la cultura lliure, participa a l'àgora aportant dubtes i coneixements!</li>
<p><a href="https://agora.fedi.cat">Registra't a l'àgora de Fedicat</a></p>
</ul>
<ul class="a">
<li><b>Microblogging federat amb Pleroma</b><br />
A més del blog federat, administrem i moderem la instància de microblogging &quot;barcelona.social&quot;, que es basa en el programari Pleroma i forma part de l'ecosistema d'eines de la Federació de mitjans socials lliures. Hem habilitat 5000 caràcters per publicació i tenim emojis personalitzats. A més, si tens un compte en aquest node, pots triar entre la interfície de Pleroma o la de Mastodon.</li>
<p><a href="https://barcelona.social/about">Registra't al node de Pleroma</a></p>
</ul>

### Eines sense registre ni peticio

<p>Gestionem algunes eines organitzatives que no requereixen registre ni petició. Fes-ne un bon ús.</p>
<ul class="a">
<li><b>Pads amb Etherpad</b><br />
Un pad és un document col·laboratiu on hi poden escriure diverses persones al mateix temps. Si cada persona que participa a un mateix pad, tria un color i un nom o pseudònim, després és més fàcil saber qui ha escrit què. Per a compartir un pad només cal compartir-ne l'enllaç.</li>
<p><a href="https://pad.laloka.org">Crea un pad</a></p>
</ul>
<ul class="a">
<li><b>Audioconferències amb Mumble</b><br />
Quan no ens cal compartir pantalla, ens comuniquem per audioconferència enlloc de per videoconferència perquè és més ecològic. A més, el programa Mumble ens permet crear sales temàtiques on debatre temes o quedar per petar-la una estona.</li>
<p><a href="https://parla.laloka.org">Fem un Parla?</a></p>
</ul>

## Eines que ens deixen

<p>Quan necessitem compartir pantalla fem servir Jitsi però no qualsevol Jitsi. El servidor oficial de projecte Jitsi, meet.jit.si, depèn d'Amazon... I quan hem de fer esdeveniments, fem servir un Big Blue Button, però no qualsevol... Tots els programes s'han d'instal·lar a un servidor, però no tots els servidors són iguals i nosaltres preferim servidors de proximitat física i ideològica que no depenguin de cap multinacional. Com sovint les veïnes d'eXO ens deixen el seu Jitsi i el seu BBB, aprofitem aquest espai per donar-els-hi les gràcies.</p>
<ul class="a">
<li><a href="https://exo.cat">Associació eXO</a></li>
<li>Programari <a href="https://jitsi.org">Jitsi</a></li>
<li>Programari <a href="https://jitsi.org">BBB</a></li>
</ul>
<h2 id="altres-eines">Altres Eines</h2>
<p>Altres programaris lliures que fem servir per crear els nostres continguts, reproduir música o vídeos, gestionar contrasenyes, navegar per les Internets i cercar-hi informació.</p>
<ul class="a">
<li>Usem sistemes operatius lliures (Ubuntu, Debian, Linux Mint, BSD, etc.)</li>
<li>Creem documents de text, fulls de càlcul i presentacions amb <a href="https://www.libreoffice.org">LibreOffice</a></li>
<li>Dibuixem vectors amb <a href="https://inkscape.org">Inkscape</a></li>
<li>Retoquem imatges amb <a href="https://www.gimp.org">GIMP</a></li>
<li>Maquetem amb <a href="https://www.scribus.net">Scribus</a></li>
<li>Reproduim arxius multimèdia amb <a href="https://www.videolan.org">VLC</a></li>
<li>Desem les contrasenyes amb <a href="https://keepassxc.org">KeepassXC</a></li>
<li>Gestionem els correus amb <a href="https://www.thunderbird.net">Thunderbird</a> o <a href="https://wiki.gnome.org/Apps/Evolution">Evolution</a></li>
<li>Naveguem per les Internets amb <a href="https://www.mozilla.org/ca/firefox/new">Firefox</a></li>
<li>Cercem informació a <a href="https://searx.space/">Searx</a></li>
<li>Ens ubiquem amb <a href="https://www.openstreetmap.org">OpenStreetMap</a></li>
<li>Experimentem nous protocols amb <a href="https://gemini.circumlunar.space">Gemini</a></li>
<li>Experimentem la moneda lliure amb un node de <a href="https://duniter.org/en">Duniter</a></li>
</ul>
