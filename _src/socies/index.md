---
layout: base-layout.njk
title: Associa't
---

Qualsevol persona interessada en la cultura lliure pot esdevenir sòcia activa o col·laboradora de LaLoka. Associar-se és una manera de mostrar el teu suport a la divulgació i creació de cultura lliure en català. Si ets una persona individual, pots escollir entre sòcia activa i sòcia col·laboradora. La quota mínima per a les persones individuals és de tres euros al mes. Si sou una entitat, també podeu fer-vos membre en qualitat d'entitat de suport.
<br />
[Estatuts de l'associacio](/associacio/estatuts/)

## Socia activa
Són sòcies actives aquelles persones que, a més de compartir els principis fundacionals de l'associació, volen participar en el dia a dia de l'associació, és a dir, en la seva gestió interna i en l'execució dels seus projectes.
<br />
[Fer-se sòcia activa](/socies/actives/)

## Socia col·laboradora
Són sòcies col·laboradores aquelles persones que comparteixen els principis fundacionals de l'associació però no estan interessades en gestionar el seu funcionament diari.
<br />
[Fer-se sòcia col·laboradora](/socies/colaboradores/)


## Entitat de suport
Si sou un col·lectiu o entitat, podeu fer-vos entitat de suport. La quota mínima per a entitats de suport és de cent euros l'any.
<br />
[Esdevenir entitat de suport](/socies/suport/)
