---
layout: base-layout.njk
title: Socia col·laboradora
---


Són sòcies col·laboradores aquelles persones que comparteixen els principis fundacionals de l'associació però no estan interessades en gestionar el seu funcionament diari. La quota mínima per ser sòcia col·laboradora és de tres euros al mes.

## Procés d'admissió

<ul>
<li>Per esdevenir sòcia col·laboradora s'ha de fer una sol·licitud i comprometre's a abonar la quota corresponent</li>
<li>La candidatura s’anunciarà a la resta de persones sòcies durant un període mínim de set dies</li>
<li>La persona sol·licitant serà acceptada com a nova sòcia a excepció que hi hagi un percentatge significatiu de sòcies que argumentin raonadament en contra durant el procés d'admissió</li>
<li>La persona que exerceixi el càrrec de secretària serà l'encarregada de tramitar l'admissió i comunicar-ne el resultat a la persona sol·licitant</li>
</ul>

## Drets i deures
Els drets i deures de les sòcies col·laboradores es detallen al capítol dos, apartat vuit dels Estatuts de l'associació.

### Drets

<p>Són drets de les sòcies col·laboradores:</p>
<ul>
<li>Estar informades de les activitats de l'associació</li>
<li>Anunciar públicament que són sòcies de LaLoka</li>
</ul>

### Deures

<p>Són deures de les sòcies col·laboradores:</p>
<ul>
<li>Abonar una quota corresponent</li>
<li>Informar amb 15 dies d’antelació del pagament de la següent quota que et vols donar de baixa de l’associació</li>
</ul>

## Procediment de baixa

<ul>
<li>Per demanar la baixa de l'associació formalment s'ha d'escriure un correu electrònic</li>
<li>Si no s'abona la quota i no es contacta amb tresoria en 15 dies es considerarà baixa de l’associació</li>
<li>Si el no abonament de la quota es justifica degudament, la persona afectada continuarà sent sòcia sense tornar a passar pel procés d’admissió</li>
</ul>

## Fes-te sòcia col·laboradora

<ul>
<li><a href="estatuts.html">Consulta els Estatuts</a></li>
<li>Fes-nos arribar la sol·licitud per ser sòcia col·laboradora (crear formulari)</li>
</ul>
<h2 id="canvia-de-modalitat">Canvia de modalitat</h2>
<p>Envia'ns un correu electrònic si vols canviar de sòcia col·laboradora a sòcia activa.</p>
