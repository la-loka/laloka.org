---
layout: base-layout.njk
title: Socia activa
---

<p>Són sòcies actives aquelles persones que, a més de compartir els principis fundacionals de l'associació, volen participar en el dia a dia de l'associació, és a dir, en la seva gestió interna i en l'execució dels seus projectes. La quota mínima per ser sòcia activa és de tres euros al mes.</p>
<h2 id="procés-dadmissió">Proces d'admissio</h2>
<ul class="a">
<li>Per esdevenir sòcia activa s'ha de fer una sol·licitud i comprometre's a abonar la quota corresponent</li>
<li>La candidatura s’anunciarà a la resta de persones sòcies durant un període mínim de set dies</li>
<li>La persona sol·licitant serà acceptada com a nova sòcia a excepció que hi hagi un percentatge significatiu de sòcies que argumentin raonadament en contra durant el procés d'admissió</li>
<li>La persona que exerceixi el càrrec de secretària serà l'encarregada de tramitar l'admissió i comunicar-ne el resultat a la persona sol·licitant</li>
</ul>
<h2 id="drets-i-deures">Drets i deures</h2>
<p>Els drets i deures de les sòcies actives es detallen al capítol dos, apartats quatre, cinc i sis dels Estatuts de l'associació.</p>
<h3 id="drets">Drets</h3>
<p>Són drets de les sòcies actives:</p>
<ul class="a">
<li>Estar informades de les activitats de l'associació</li>
<li>Anunciar públicament que són sòcies de LaLoka</li>
<li>Tenir veu i vot en les assemblees generals, i participar de comissions i grups de treball</li>
<li>Vetar de manera argumentada l’admissió de noves sòcies si consideren que es posa en joc els fonaments de l'associació</li>
<li>Prèvia consulta i aprovació, emprendre iniciatives aliniades amb els objectius i accions de l'associació</li>
</ul>
<h3 id="deures">Deures</h3>
<p>Són deures de les sòcies actives:</p>
<ul class="a">
<li>Comprometre's amb les finalitats de l'associació i participar activament per assolir-les</li>
<li>Respectar els acords de convivència</li>
<li>Abonar la quota corresponent</li>
<li>Informar amb 15 dies d’antelació del pagament de la següent quota que et vols donar de baixa de l’associació</li>
</ul>
<h2 id="procediment-de-baixa">Procediment de baixa</h2>
<ul class="a">
<li>Per demanar la baixa de l'associació formalment s'ha d'escriure un correu electrònic</li>
<li>Si no s'abona la quota i no es contacta amb tresoria en 15 dies es considerarà baixa de l’associació</li>
<li>Si el no abonament de la quota es justifica degudament, la persona afectada continuarà sent sòcia sense tornar a passar pel procés d’admissió</li>
</ul>
<h2 id="fes-te-socia-activa">Fes-te socia activa</h2>
<ul class="a">
<li><a href="estatuts.html">Consulta els Estatuts</a></li>
<li>Fes-nos arribar la sol·licitud per ser sòcia activa (enllaç formulari)</li>
</ul>
<h2 id="canvia-de-modalitat">Canvia de modalitat</h2>
<p>Envia'ns un correu electrònic si vols canviar de sòcia activa a sòcia col·laboradora.</p>
