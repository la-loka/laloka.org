---
layout: base-layout.njk
title: Entitat de suport
---

<p>Si sou un col·lectiu o entitat, podeu fer-vos entitat de suport. La quota mínima per a entitats de suport és de cent euros l'any.</p>
<h2 id="proces-dadmissio">Proces d'admissio</h2>
<ul class="a">
<li>Per esdevenir entitat de suport membre s'ha de fer una sol·licitud i comprometre's a abonar la quota corresponent</li>
<li>La candidatura s’anunciarà a la resta de persones sòcies durant un període mínim de set dies</li>
<li>El col·lectiu o entitat sol·licitant serà acceptada com a nova sòcia a excepció que hi hagi un percentatge significatiu de sòcies que argumentin raonadament en contra durant el procés d'admissió</li>
<li>La persona que exerceixi el càrrec de secretària serà l'encarregada de tramitar l'admissió i comunicar-ne el resultat al col·lectiu o entitat sol·licitant</li>
</ul>
<h2 id="drets-i-deures">Drets i deures</h2>
<p>Els drets i deures de les entitats de suport es detallen al capítol dos, apartat vuit dels Estatuts de l'associació.</p>
<h3 id="drets">Drets</h3>
<p>Són drets de les entitats de suport:</p>
<ul class="a">
<li>Estar informades de les activitats de l'associació</li>
<li>Anunciar públicament que són sòcies de LaLoka</li>
</ul>
<h3 id="deures">Deures</h3>
<p>Són deures de les entitats de suport:</p>
<ul class="a">
<li>Abonar la quota corresponent</li>
<li>Informar amb 15 dies d’antelació del pagament de la següent quota que es volen donar de baixa de l’associació, sense perjudici que es pugui admetre candidatura de nou pel procediment establert</li>
</ul>
<h2 id="procediment-de-baixa">Procediment de baixa</h2>
<ul class="a">
<li>Per demanar la baixa de l'associació formalment s'ha d'escriure un correu electrònic</li>
<li>Si no s'abona la quota corresponent i no es contacta amb tresoria en 15 dies es considerarà baixa de l’associació</li>
<li>Si el no abonament de la quota es justifica degudament, l'entitat afectada continuarà sent sòcia sense tornar a passar pel procés d’admissió</li>
</ul>
<h2 id="feu-vos-entitat-de-suport">Feu-vos entitat de suport</h2>
<ul class="a">
<li><a href="estatuts.html">Consulteu els Estatuts</a></li>
<li>Feu-nos arribar la sol·licitud per ser entitat de suport (crear formulari)</li>
</ul>
