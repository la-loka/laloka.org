# laloka.org static site generator


This static site generator uses [Eleventy] and requires `node.js`

## Install node.js

https://joshtronic.com/2020/04/21/how-to-install-nodejs-14-on-debian-10/

## Clone the project

```
git clone git@gitlab.com:la-loka/laloka.org.git
```

## Install Eleventy

```
cd laloka.org
npm install @11ty/eleventy
```

## Start the Eleventy server

```
npx eleventy --serve
```

You can now browse http://localhost:8080


## Edit laloka.org

The markdown files are in the `_src` directory. Edit those files.

The HTML static site is automagically generated in `_site`. Do not edit those files.


[Eleventy]: https://www.11ty.dev/
