module.exports = function(eleventyConfig) {

  // Copy the `img` and `css` folders to the output
  eleventyConfig.addPassthroughCopy("static");
    return {
    dir: {
      input: '_src',
      output: '_site'
    }
  };
}
